<?php

/**
 * PHPSTORM_META functions stub for static analysis
 *
 * This file is derived from meta/.phpstorm.meta.php of jetbrains/phpstorm-stubs.
 *
 * @copyright JetBrains s.r.o.
 * @license Apache-2.0
 * @link https://github.com/JetBrains/phpstorm-stubs
 * @link https://github.com/JetBrains/phpstorm-stubs/blob/2d60e98/composer.json
 * @link https://github.com/JetBrains/phpstorm-stubs/blob/f70f79c/meta/.phpstorm.meta.php
 */
namespace PHPSTORM_META;

/**
 * @param mixed $funcall Class, Method or function call. Its argument is always 0.
 * @param mixed $method one of {@see map()}, {@see type()}, {@see elementType()}.
 * @return mixed override pair object
 */
function override($funcall, $override) {
    return "override $funcall $override";
}

/**
 * map argument with #$argNum Literal value to one of expressions
 *
 * @param array<mixed,string> $map Key-value pairs: string_literal|const|class_const => class_name::class|pattern_literal
 * where pattern literal can contain @ char to be replaced with argument literal value
 * @phan-param array<string|int,class-string|string> $map
 * @return mixed overrides map object
 */
function map(array $map) {
    return "map $map";
}

/**
 * type of argument #$argNum
 * @param mixed $argNum ignored, for now its always 0
 * @return mixed
 */
function type($argNum) {
    return "type $argNum";
}

/**
 * element type of argument #$argNum
 * @param mixed $argNum
 * @return mixed
 */
function elementType($argNum) {
    return "elementType $argNum";
}

function expectedArguments($functionReference, $argumentIndex, ...$values) {
    return "expectedArguments " . $functionReference . "at " . $argumentIndex . ": " . implode(', ', $values);
}

function registerArgumentsSet($setName, ...$values) {
    return "registerArgumentsSet " . $setName . ": "  . implode(', ', $values);
}

function argumentsSet($setName) {
    return "argumentsSet " . $setName;
}

function expectedReturnValues($functionReference, ...$values) {
    return "expectedReturnValues " . $functionReference . ": " . implode(', ', $values);
}
