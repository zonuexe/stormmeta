<?php

namespace StormMeta;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

/**
 * Base TestCase for StormMeta
 *
 * @author USAMI Kenta <tadsan@zonu.me>
 * @copyright 2019 USAMI Kenta
 * @license https://www.mozilla.org/en-US/MPL/2.0/ MPL-2.0
 */
abstract class TestCase extends PHPUnitTestCase
{
}
